import { useContext } from "react";
import { PostsContext } from "../contexts/postsContext";

export function Home() {
  const { total } = useContext(PostsContext);

  return (
    <>
      <h1>Hello, from Home Page </h1>
      <p>Total posts: {total}</p>
    </>
  );
}
