import { useContext, useState } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import { UserContext } from "../contexts/UserContext";

export function Login() {
  const { setUser, user } = useContext(UserContext);
  const [submitting, setSubmitting] = useState(false);
  const navigate = useNavigate();

  if (user) {
    return <Navigate to="/" />;
  }

  function handleSubmit(event) {
    event.preventDefault();
    const { username, password } = event.target.elements;

    setSubmitting(true);
    fetch("https://dummyjson.com/auth/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: username.value,
        password: password.value,
      }),
    })
      .then((res) => res.json())
      .then((user) => {
        setUser(user);
        localStorage.setItem("user_data", JSON.stringify(user));
        navigate("/");
      })
      .finally(() => {
        setSubmitting(false);
      });
  }

  return (
    <div className="container">
      <h1 className="text-center">Log In</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-2">
          <label className="form-label" htmlFor="username">
            Username:
          </label>
          <input
            id="username"
            type="text"
            className="form-control"
            placeholder="Enter username"
          />
        </div>
        <div className="mb-2">
          <label className="form-label" htmlFor="password">
            Password:
          </label>
          <input
            id="password"
            type="password"
            className="form-control"
            placeholder="Enter password"
          />
        </div>
        <div className="mb-2">
          <button disabled={submitting} className="btn btn-success btn-block">
            Login
          </button>
        </div>
      </form>
    </div>
  );
}
