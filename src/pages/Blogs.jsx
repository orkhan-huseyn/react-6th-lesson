import { useContext } from "react";
import { Link } from "react-router-dom";
import { PostsContext } from "../contexts/postsContext";

export function Blogs() {
  const { posts } = useContext(PostsContext);

  return (
    <>
      <h1>Hello, from Blogs Page </h1>
      <ol className="list-group">
        {posts.map((post) => (
          <li
            key={post.id}
            className="list-group-item d-flex justify-content-between align-items-start"
          >
            <div className="ms-2 me-auto">
              <Link to={`/blogs/${post.id}`}>
                <div className="fw-bold">{post.title}</div>
              </Link>
              {post.tags.join(", ")}
            </div>
            <span className="badge text-bg-primary rounded-pill">
              {post.reactions.likes}
            </span>
          </li>
        ))}
      </ol>
    </>
  );
}
