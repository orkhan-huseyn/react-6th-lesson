import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export function BlogDetails() {
  const { blogId } = useParams();
  const [post, setPost] = useState(null);

  useEffect(() => {
    fetch(`https://dummyjson.com/posts/${blogId}`)
      .then((res) => res.json())
      .then((res) => {
        setPost(res);
      });
  }, []);

  if (post == null) {
    return <h1>Loading... </h1>;
  }

  return (
    <article>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
    </article>
  );
}
