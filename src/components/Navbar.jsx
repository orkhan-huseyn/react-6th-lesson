import { useContext } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { UserContext } from "../contexts/UserContext";

export function Navbar() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  function logOut() {
    localStorage.removeItem("user_data");
    navigate("/login");
  }

  return (
    <nav className="navbar bg-body-tertiary">
      <ul className="navbar-nav">
        <li className="nav-item">
          <NavLink className="nav-link" to="/">
            Home
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/blogs">
            Blogs
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/pricing">
            Pricing
          </NavLink>
        </li>
        <li className="nav-item">@{user.username}</li>
        <li className="nav-item">
          <button onClick={logOut} className="nav-link">
            Log Out
          </button>
        </li>
      </ul>
    </nav>
  );
}
