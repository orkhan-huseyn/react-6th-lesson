export function Footer() {
  return (
    <footer className="navbar bg-body-tertiary">
      <div className="container-fluid">
        <span className="navbar-text">Copyright 2024 ECAMP</span>
      </div>
    </footer>
  );
}
