import { createContext, useState } from "react";

export const UserContext = createContext();

const getExistingUser = () => {
  const existingUser = localStorage.getItem("user_data");
  if (existingUser) {
    return JSON.parse(existingUser);
  }
  return null;
};

export function UserProvider({ children }) {
  const [user, setUser] = useState(getExistingUser);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      {children}
    </UserContext.Provider>
  );
}
