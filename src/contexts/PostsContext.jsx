import { createContext, useState, useEffect } from "react";

export const PostsContext = createContext();

export function PostsProvider({ children }) {
  const [posts, setPosts] = useState([]);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    fetch("https://dummyjson.com/posts")
      .then((res) => res.json())
      .then((res) => {
        setPosts(res.posts);
        setTotal(res.total);
      });
  }, []);

  return (
    <PostsContext.Provider value={{ posts, total }}>
      {children}
    </PostsContext.Provider>
  );
}
