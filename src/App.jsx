import { RouterProvider } from "react-router-dom";
import { router } from "./routes";
import { PostsProvider } from "./contexts/postsContext";
import { UserProvider } from "./contexts/UserContext";

export default function App() {
  return (
    <UserProvider>
      <PostsProvider>
        <RouterProvider router={router} />
      </PostsProvider>
    </UserProvider>
  );
}
